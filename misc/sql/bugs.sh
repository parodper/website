#!/bin/sh
# query fo rbugs
# Used to rewrite bugs pages for web sentinel based on UDD query only

if [ $# -lt 1 ] ; then
    echo "Usage: $0 <blendname>"
    exit
fi

psql udd > $1_buggy_packages.out <<EOT
      SELECT distinct sources.source, task, CASE WHEN (tasks.dependency = 'd' OR tasks.dependency = 'r') AND component = 'main' AND experimental_flag > 0 THEN 'depends' ELSE 'suggests' END AS status,
                      homepage, CASE WHEN vcs_browser IS NULL THEN '#' ELSE vcs_browser END AS vcs_browser, maintainer
        FROM (
          SELECT DISTINCT s.source, b.dependency, b.component, s.homepage, s.vcs_browser, s.maintainer, s.version
          FROM (
            SELECT DISTINCT source, dependency, component, version FROM (
              SELECT p.package, source, bd.dependency, bd.component, strip_binary_upload(version) AS version,
                     row_number() OVER (PARTITION BY p.package ORDER BY p.version DESC)
                FROM blends_dependencies bd
                JOIN packages p ON p.package = bd.package
                WHERE blend = '$1' AND bd.distribution = 'debian'
                -- GROUP BY p.package, p.source, bd.dependency, bd.component, p.version
              ) tmp
              WHERE row_number = 1
            ) b
            JOIN sources s  ON s.source  = b.source AND strip_binary_upload(s.version) = b.version
            JOIN bugs bu    ON bu.source = b.source
        ) sources
        -- check status of dependency relation because only suggested packages are less important for bugs sentinel
        LEFT OUTER JOIN (
          SELECT source, task, dependency FROM (
            SELECT p.source, b.task, bdp.dependency, row_number() OVER (PARTITION BY p.source, b.task ORDER BY bdp.priority)
              FROM packages p
              JOIN blends_dependencies b ON b.package = p.package
              JOIN sources s ON p.source = s.source AND p.release = s.release
              JOIN blends_dependencies_priorities bdp ON b.dependency = bdp.dependency
              WHERE b.blend = '$1'
              -- GROUP BY p.source, b.task, bdp.dependency, bdp.priority
          ) tmp
          WHERE row_number = 1
        ) tasks ON sources.source = tasks.source
        -- Check, whether a package is in experimental only which makes it less important for bugs sentinel
        LEFT OUTER JOIN (
          SELECT source, MAX(sort) AS experimental_flag FROM sources s
            JOIN releases r ON s.release = r.release
          GROUP BY source
        ) exp ON sources.source = exp.source
        ORDER BY source;
EOT

exit

psql udd >> $1_buggy_packages.out <<EOT
      SELECT source, bu.id, title, severity, status, done_by, tags FROM (
        SELECT distinct bu.source, bu.id, bu.title, bu.severity, bu.status, bu.done AS done_by
          FROM blends_dependencies b
          JOIN packages p ON p.package = b.package
          JOIN bugs bu    ON bu.source = p.source
          WHERE blend = '$1' AND b.distribution = 'debian'
        ) bu
        LEFT OUTER JOIN (
          SELECT id, array_agg(tag) AS tags FROM bugs_tags GROUP BY id
        ) bt ON bu.id = bt.id
        ORDER BY source, bu.id;
EOT


#!/bin/sh

pybabel extract \
    --mapping-file=babel.cfg \
    --msgid-bugs-address="debian-blends@lists.debian.org" \
    --copyright-holder="Debian Pure Blends Team <debian-blends@lists.debian.org>" \
    --output=po/blends-webtools.pot \
    $(dirname $0)

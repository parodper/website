# -*- coding: utf-8 -*-
'''Define languages which are supported in web sentinel pages
Copyright 2010: Andreas Tille <tille@debian.org>
License: GPL
'''

# The list contains dictionaries with the following meaning:
#  'ddtp'     = address the translation files of DDTP
#  'short'    = shortcut of language as extension of the output files
#  'htaccess' = language definition in htaccess
#  'title'    = English name of the language used in title attribut of link
#  'printed'  = printed name for links
languages = [
    {'ddtp': 'en', 'short': 'en', 'htaccess': 'en',
     'title': 'English', 'printed': u'English'},
    {'ddtp': 'cs', 'short': 'cs', 'htaccess': 'cs',
     'title': 'Czech', 'printed': u'česky'},
    {'ddtp': 'da', 'short': 'da', 'htaccess': 'da',
     'title': 'Danish', 'printed': u'dansk'},
    {'ddtp': 'de', 'short': 'de', 'htaccess': 'de',
     'title': 'German', 'printed': u'Deutsch'},
    {'ddtp': 'es', 'short': 'es', 'htaccess': 'es',
     'title': 'Spanish', 'printed': u'español'},
    {'ddtp': 'fi', 'short': 'fi', 'htaccess': 'fi',
     'title': 'Finnish', 'printed': u'suomi'},
    {'ddtp': 'fr', 'short': 'fr', 'htaccess': 'fr',
     'title': 'French', 'printed': u'français'},
    {'ddtp': 'hu', 'short': 'hu', 'htaccess': 'hu',
     'title': 'Hungarian', 'printed': u'magyar'},
    {'ddtp': 'it', 'short': 'it', 'htaccess': 'it',
     'title': 'Italian', 'printed': u'Italiano'},
    {'ddtp': 'ja', 'short': 'ja', 'htaccess': 'ja',
     'title': 'Japanese', 'printed': u'日本語 (Nihongo)'},
    {'ddtp': 'ko', 'short': 'ko', 'htaccess': 'ko',
     'title': 'Korean', 'printed': u'한국어 (Hangul)'},
    {'ddtp': 'nl', 'short': 'nl', 'htaccess': 'nl',
     'title': 'Dutch', 'printed': u'Nederlands'},
    {'ddtp': 'pl', 'short': 'pl', 'htaccess': 'pl',
     'title': 'Polish', 'printed': u'polski'},
    {'ddtp': 'pt_BR', 'short': 'pt', 'htaccess': 'pt-BR',
     'title': 'Portuguese', 'printed': u'Português'},
    {'ddtp': 'ru', 'short': 'ru', 'htaccess': 'ru',
     'title': 'Russian', 'printed': u'Русский (Russkij)'},
    {'ddtp': 'uk', 'short': 'uk', 'htaccess': 'uk',
     'title': 'Ukrainian', 'printed': u"українська (ukrajins'ka)"},
    {'ddtp': 'sk', 'short': 'sk', 'htaccess': 'sk',
     'title': 'Slovenian', 'printed': u'slovensky'},
    {'ddtp': 'sr', 'short': 'sr', 'htaccess': 'sr',
     'title': 'Serbian', 'printed': u'српски (srpski)'},
    {'ddtp': 'sv', 'short': 'sv', 'htaccess': 'sv',
     'title': 'Swedish', 'printed': u'svenska'},
    {'ddtp': 'vi', 'short': 'vi', 'htaccess': 'vi',
     'title': 'Vietnamese', 'printed': u'Tiếng Việt'},
    {'ddtp': 'zh_CN', 'short': 'zh_cn', 'htaccess': 'zh-CN',
     'title': 'Chinese (China)', 'printed': u'中文(简)'},
    {'ddtp': 'zh_TW', 'short': 'zh_tw', 'htaccess': 'zh-TW',
     'title': 'Chinese (Taiwan)', 'printed': u'中文(正)'},
]
